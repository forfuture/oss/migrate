"use strict";
/**
 * The MIT License (MIT)
 * Copyright (c) 2017 Forfuture, LLC <we@forfuture.co.ke>
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const main_1 = require("./main");
Object.defineProperty(exports, "main", { enumerable: true, get: function () { return main_1.main; } });
//# sourceMappingURL=index.js.map